'use strict';

module.exports = function (grunt) {
    var watchedFiles = ['./views/**/*.*',
        'routes/**/*.js',
        'public/**/*.*',
        'models/**/*.js',
        'controllers/**/*.js',
        'config/**/*.js',
        'app.js',
        'server.js',
        'Gruntfile.js'
    ];

    grunt.initConfig({
        nodemon: {
            dev: {
                script: 'server.js',
                options: {
                    nodeArgs: ['--debug'],
                    ext: 'js,html,ejs',
                    watch: watchedFiles
                }
            }
        },
        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                    quiet: false, // Optionally suppress output to standard out (defaults to false)
                    clearRequireCache: true // Optionally clear the require cache before running tests (defaults to false)
                },
                src: ['tests/**/*.js']
            }
        }
    });

    /**
     * nodemon grunt plugin
     */
    grunt.loadNpmTasks('grunt-nodemon');

    /**
     * mocha grunt plugin
     */
    grunt.loadNpmTasks('grunt-mocha-test');

    /**
     * run the application
     */
    grunt.registerTask('default', ['nodemon']);

    /**
     * run tests
     */
    grunt.registerTask('test', ['mochaTest']);
};