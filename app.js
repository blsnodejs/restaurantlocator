'use strict';

/**
 * this is an initialization script that will correctly set NODE_ENV which in turn will
 * decide which config file to use
 */
require('./config/init')();

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var debug = require('debug')('RestaurantLocator:server');
var http = require('http');
var chalk = require('chalk');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var passport = require('passport');
var glob = require('glob');
var session = require('express-session');
var mongoStore = require('connect-mongo')({
    session: session
});

var config = require('./config/config');

/**
 * this will connect to the database, if for some reason the database cannot be reached then we should stop
 * and throw and exception since the APIs heavily rely on the database.
 */
var db = mongoose.connect(config.db, function (err) {
    if (err) {
        console.error(chalk.red('Could not connect to database'));
        throw err;
    }
});

var app = express();

/**
 * disables the "X-Powered-By: Express" HTTP header.
 */
app.disable('x-powered-by');
/**
 * just in case the app is behind proxy
 */
app.enable('trust proxy');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: config.sessionSecret,
    store: new mongoStore({
        mongooseConnection: db.connection,
        collection: config.sessionCollection
    })
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

/**
 * auto load the models
 */
glob('./models/*.js', {
    sync: true
}, function (err, modelFiles) {
    if (modelFiles.length) {
        for (var i = 0; i < modelFiles.length; i++) {
            var currentModelFile = modelFiles[i];
            require(path.resolve(currentModelFile));
        }
    }
});
/**
 * The routes needs to be placed under body parser middleware in order for the body parser middleware to be applied
 */
/**
 * auto load the routes
 */
glob('./routes/*.js', {
    sync: true
}, function (err, routeFiles) {
    if (routeFiles.length) {
        for (var i = 0; i < routeFiles.length; i++) {
            var currentRouteFile = routeFiles[i];
            require(path.resolve(currentRouteFile))(app);
        }
    }
});

require('./config/passport')();

app.get('/', function (req, res) {
    res.render('index', {
        title: config.app.title
    })
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {}
    });
});

module.exports = app;