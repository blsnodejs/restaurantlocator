'use strict';

module.exports = {
    db: 'mongodb://localhost/restaurantlocator',
    app: {
        title: 'Restaurant Locator'
    },
    facebook: {
        clientID: 'APP_ID',
        clientSecret: 'APP_ID',
        callbackURL: '/auth/facebook/callback'
    },
    twitter: {
        clientID: 'APP_ID',
        clientSecret: 'APP_ID',
        callbackURL: '/auth/twitter/callback'
    },
    google: {
        clientID: 'APP_ID',
        clientSecret: 'APP_ID',
        callbackURL: '/auth/google/callback'
    },
    mailer: {
        enabled: false,
        from: 'MAILER_FROM',
        options: {
            service: 'MAILER_SERVICE_PROVIDER',
            auth: {
                user: 'MAILER_EMAIL_ID',
                pass: 'MAILER_PASSWORD'
            }
        }
    }
};
