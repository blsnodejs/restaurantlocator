'use strict';

module.exports = {
    db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/restaurantlocator',
    app: {
        title: 'Restaurant Locator'
    },
    facebook: {
        clientID: 'APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: '/auth/facebook/callback'
    },
    twitter: {
        clientID: 'CONSUMER_KEY',
        clientSecret: 'CONSUMER_SECRET',
        callbackURL: '/auth/twitter/callback'
    },
    google: {
        clientID: 'APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: '/auth/google/callback'
    },
    mailer: {
        enabled: false,
        from: 'MAILER_FROM',
        options: {
            service: 'MAILER_SERVICE_PROVIDER',
            auth: {
                user: 'MAILER_EMAIL_ID',
                pass: 'MAILER_PASSWORD'
            }
        }
    }
};
