'use strict';

var glob = require('glob'),
    chalk = require('chalk'),
    path = require('path');

module.exports = function () {
    glob('./env/' + process.env.NODE_ENV + '.js', {
        sync: true
    }, function (err, envFiles) {
        if (envFiles.length) {
            console.log(chalk.black.bgWhite('The APIs will be operating according to: ' + process.env.NODE_ENV + ' environment configuration file.'));
        } else {
            if (process.env.NODE_ENV) {
                console.error(chalk.red('No configuration file found for: ' + process.env.NODE_ENV));
            } else {
                console.error(chalk.red('NODE_ENV not set, will be defaulting to development'));
            }

            process.env.NODE_ENV = 'development';
        }
    });
};