'use strict';

var passport = require('passport'),
    User = require('mongoose').model('User'),
    path = require('path'),
    config = require('./config'),
    glob = require('glob');

module.exports = function () {
    // Serialize sessions
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // Deserialize sessions
    passport.deserializeUser(function (id, done) {
        User.findOne({
            _id: id
        }, '-salt -password', function (err, user) {
            done(err, user);
        });
    });

    // Initialize strategies
    glob('./config/strategies/**/*.js', {
        sync: true
    }, function (err, strategyFile) {
        if (strategyFile.length) {
            strategyFile.forEach(function (strategy) {
                require(path.resolve(strategy))();
            });
        }
    });
};