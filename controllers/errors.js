'use strict';

var messages = {
    Generic: {
        '100': 'Empty Response.',
        '101': 'Validation failed'
    },
    Restaurants: {
        '100': 'Restaurant ID is required.',
        '101': 'Cuisine type parameter is required.',
        '102': 'Some or all proximity parameters are missing.'
    },
    Users: {
        '100': 'User is not authorized',
        '101': 'No account with that username has been found',
        '102': 'Username field must not be blank',
        '103': 'Password reset token is invalid or has expired.',
        '104': 'Your password has been changed',
        '105': 'User could not be found with the supplied reset token',
        '106': 'Reset password token is valid.',
        '107': 'Passwords do not match',
        '108': 'User is not signed in',
        '109': 'Please provide a new password',
        '110': 'User is not found',
        '111': 'Current password is incorrect',
        '112': 'Passwords do not match',
        '113': 'Password changed successfully',
        '114': 'The User id parameter is required.',
        '115': 'Username not found'
    },
    FoodOrders: {
        '100': 'The User ID is required.',
        '101': 'The restaurant ID is required.',
        '102': 'The food order ID is required.'
    },
    RestaurantReviews: {
        '100': 'Missing body information to create a restaurant review.',
        '101': 'The restaurant ID parameter is required.',
        '102': 'The user ID parameter is required.',
        '103': 'The restaurant review ID parameter is required.'
    }
};

var getUniqueErrorMessage = function (err) {
    var output;

    try {
        var fieldName = err.err.substring(err.err.lastIndexOf('.$') + 2, err.err.lastIndexOf('_1'));
        output = fieldName.charAt(0).toUpperCase() + fieldName.slice(1) + ' already exists';

    } catch (ex) {
        output = 'Unique field already exists';
    }

    return output;
};

/**
 * Get the error message from error object
 */
exports.getErrorMessage = function (err, type, code) {
    var message = '';

    if (arguments) {
        if (arguments.length == 1) {
            if (err.code) {
                switch (err.code) {
                    case 11000:
                    case 11001:
                        message = getUniqueErrorMessage(err);
                        break;
                    case 16572:
                        message = err.message;
                        break;
                    default:
                        message = 'Something went wrong';
                }
            } else if (err.message) {
                message = err.message;
            } else {
                for (var errName in err.errors) {
                    if (err.errors[errName].message) message = err.errors[errName].message;
                }
            }
        } else if (arguments.length === 3) {
            message = messages[type][code];
        }
    }

    return message;
};

exports.rawMessages = messages;