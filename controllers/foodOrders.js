'use strict';

var mongoose = require('mongoose'),
    _ = require('lodash'),
    FoodOrder = mongoose.model('FoodOrder'),
    errorsHandler = require('../controllers/errors');

exports.getUserFoodOrders = function (req, res) {
    var userId = req.profile.id;
    FoodOrder.find({
        userId: userId
    }, function (err, foodOrders) {
        if (err) {
            return res
                .status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        if (!foodOrders || !foodOrders.length) {
            return res.send({
                message: errorsHandler.getErrorMessage(null, 'Generic', '100')
            });
        }
        return res.json(foodOrders);
    });
};

exports.getOrdersOfRestaurantId = function (req, res) {
    var restaurantId = req.params.restaurantId;
    FoodOrder.find({
        restaurantId: restaurantId
    }, function (err, foodOrders) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        if (!foodOrders || !foodOrders.length) {
            return res.send({
                message: errorsHandler.getErrorMessage(null, 'Generic', '100')
            });
        }
        return res.json(foodOrders);
    });
};

exports.addFoodOrder = function (req, res) {
    var userId = req.profile.id;
    var restaurantId = req.params.restaurantId;

    var foodOrder = req.body;
    /**
     * this is used in order to override the case where a user provides a userid and/or restaurantid
     */
    foodOrder.userId = userId;
    foodOrder.restaurantId = restaurantId;

    var newFoodOrder = new FoodOrder(foodOrder);
    newFoodOrder.save(function (err, response) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        return res.json(response);
    });
};

exports.deleteFoodOrder = function (req, res) {
    var foodOrderId = req.body.foodOrderId;
    if (_.isUndefined(foodOrderId) || _.isNull(foodOrderId)) {
        return res.status(400)
            .send({
                message: errorsHandler.getErrorMessage(null, 'FoodOrders', '102')
            });
    }
    FoodOrder.findByIdAndRemove(foodOrderId, function (err, doc) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        return res.json(doc);
    });
};

exports.getFoodOrderById = function (req, res) {
    var foodOrderId = req.params.foodOrderId;
    FoodOrder.findById(foodOrderId, function (err, foodOrder) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        if (!foodOrder) {
            return res.send({
                message: errorsHandler.getErrorMessage(null, 'Generic', '100')
            });
        }
        return res.json(foodOrder);
    });
};