'use strict';

var mongoose = require('mongoose'),
    _ = require('lodash'),
    RestaurantReview = mongoose.model('RestaurantReview'),
    errorsHandler = require('../controllers/errors');

exports.getReviewsByRestaurantId = function (req, res) {
    var restaurantId = req.params.restaurantId;
    RestaurantReview.find({
        restaurantId: restaurantId
    }, function (err, reviews) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        if (!reviews || !reviews.length) {
            return res.send({
                message: errorsHandler.getErrorMessage(null, 'Generic', '100')
            });
        }
        return res.json(reviews);
    });
};

exports.addRestaurantReview = function (req, res) {
    var restaurantId = req.body.restaurantId;
    if (_.isUndefined(restaurantId) || _.isNull(restaurantId)) {
        return res.status(400)
            .send({
                message: errorsHandler.getErrorMessage(null, 'RestaurantReviews', '101')
            });
    }

    var userId = req.body.userId;
    if (_.isUndefined(userId) || _.isNull(userId)) {
        return res.status(400)
            .send({
                message: errorsHandler.getErrorMessage(null, 'RestaurantReviews', '102')
            });
    }

    var restaurantReview = new RestaurantReview(req.body);
    restaurantReview.save(function (err, review) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        return res.json(review);
    });
};

exports.deleteRestaurantReview = function (req, res) {
    var reviewId = req.body.id;
    if (_.isUndefined(reviewId) || _.isNull(reviewId)) {
        return res.status(400)
            .send({
                message: errorsHandler.getErrorMessage(null, 'RestaurantReviews', '103')
            });
    }

    RestaurantReview.findOneAndRemove({
        userId: req.user._id,
        _id: reviewId
    }, function (err, review) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorsHandler.getErrorMessage(err)
                });
        }
        return res.json(review);
    });
};