'use strict';

var mongoose = require('mongoose'),
    _ = require('lodash'),
    Restaurant = mongoose.model('Restaurant'),
    RestaurantBranch = mongoose.model('RestaurantBranch'),
    errorHandler = require('./errors');

exports.listEnabled = function (req, res) {
    Restaurant.find({status: true}, function (err, restaurants) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        if (!restaurants || !restaurants.length) {
            return res.send({
                message: errorHandler.getErrorMessage(null, 'Generic', '100')
            });
        }
        return res.json(restaurants);
    });
};

exports.listAll = function (req, res) {
    Restaurant.find(function (err, restaurants) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        if (!restaurants || !restaurants.length) {
            return res.send({
                message: errorHandler.getErrorMessage(null, 'Generic', '100')
            });
        }
        return res.json(restaurants);
    });
};

exports.create = function (req, res) {
    var restaurant = new Restaurant(req.body);

    restaurant.save(function (err, restaurant) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(restaurant);
        }
    });
};

exports.update = function (req, res) {
    var restaurant = req.restaurant;

    restaurant = _.extend(restaurant, req.body);

    restaurant.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        return res.json(restaurant);
    });
};

exports.delete = function (req, res) {
    var restaurant = req.restaurant;
    Restaurant.findByIdAndRemove(restaurant._id, function (err, doc) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        delete req.restaurant;
        res.status(200);
        return res.json(doc);
    });
};

exports.getRestaurantById = function (req, res) {
    return res.json(req.restaurant);
};

exports.getRestaurantByCuisine = function (req, res) {
    var cuisineType = req.params.cuisineType;
    if (_.isUndefined(cuisineType)) {
        return res.status(400)
            .send({
                message: errorHandler.getErrorMessage(null, 'Restaurant', '101')
            });
    }
    Restaurant.find({cuisine: cuisineType}, function (err, restaurants) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        return res.json(restaurants);
    });
};

exports.getAvailableCuisines = function (req, res) {
    Restaurant.distinct('cuisine', function (err, cuisines) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        return res.json(cuisines);
    });
};

exports.getRestaurantByProximity = function (req, res) {
    var lat = req.query.lat,
        long = req.query.long,
        proximity = req.query.proximityRange;
    if (_.isUndefined(lat) || _.isUndefined(long) || _.isUndefined(proximity)) {
        return res.status(400)
            .send({
                message: errorHandler.getErrorMessage(null, 'Restaurants', '102')
            });
    }
    RestaurantBranch.where('loc').near({
        center: [long, lat],
        maxDistance: proximity / 6371000,
        spherical: true
    }).exec(function (err, branches) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        return res.json(branches);
    });
};

exports.listBranches = function (req, res) {
    var restaurant = req.restaurant;
    RestaurantBranch.find({parentId: restaurant._id}, function (err, branches) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        return res.json(branches);
    });
};

exports.addBranch = function (req, res) {
    var obj = {parentId: req.restaurant._id};
    var restaurantBranch = new RestaurantBranch(_.extend(obj, req.body));
    restaurantBranch.save(function (err, newBranch) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        return res.json(newBranch);
    });
};

exports.updateBranch = function (req, res) {
    var obj = {parentId: req.restaurant._id};
    var branchToUpdate = _.extend(obj, req.body);
    delete branchToUpdate._id;
    RestaurantBranch.findByIdAndUpdate(req.body._id, branchToUpdate, function (err, updatedBranch) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        return res.json(updatedBranch);
    });
};

exports.deleteBranch = function (req, res) {
    var obj = {parentId: req.restaurant._id};
    var branchToDelete = _.extend(obj, req.body);
    RestaurantBranch.findByIdAndRemove(branchToDelete._id, function (err, deletedBranch) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        return res.json(deletedBranch);
    });
};

/**
 * Restaurant Middleware
 */
exports.restaurantById = function (req, res, next, restaurantId) {
    Restaurant.findById(restaurantId, function (err, restaurant) {
        if (err) return next(err);
        if (!restaurant) return next(new Error('Restaurant id: ' + restaurantId + ' not found'));
        req.restaurant = restaurant;
        next();
    });
};