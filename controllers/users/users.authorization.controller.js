'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    passport = require('passport'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    errorHandler = require('../errors');

/**
 * User middleware
 */
exports.userByID = function (req, res, next, id) {
    User.findOne({
        _id: id
    }).exec(function (err, user) {
        if (err) return next(err);
        if (!user) return next(new Error('Failed to load User ' + id));
        req.profile = user;
        next();
    });
};

/**
 * Require login routing middleware
 */
exports.requiresLogin = function (req, res, next) {
    if (!req.isAuthenticated()) {
        return res.status(401).send({
            message: 'User is not logged in'
        });
    }

    next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function (roles) {
    var _this = this;

    return function (req, res, next) {
        _this.requiresLogin(req, res, function () {
            if (_.intersection(req.user.roles, roles).length) {
                return next();
            } else {
                return res.status(401).send({
                    message: errorHandler.getErrorMessage(null, 'Users', '100')
                });
            }
        });
    };
};

exports.isAdminUser = function (req, res, next) {
    if (!req.user) {
        return res.status(401)
            .send({
                message: errorHandler.getErrorMessage(null, 'Users', '100')
            });
    }
    if (req.user.roles.indexOf('admin') !== -1) {
        next();
    } else {
        return res.status(401).send({
            message: errorHandler.getErrorMessage(null, 'Users', '100')
        });
    }
};

exports.isMobileOrAdmin = function (req, res, next) {
    passport.authenticate('bearer', {session: false}, function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (user) {
            req.user = user;
            return next(null, user);
        } else {
            if (!req.user) {
                return res.status(401)
                    .send({
                        message: errorHandler.getErrorMessage(null, 'Users', '100')
                    });
            }
            if (req.user.roles.indexOf('admin') !== -1) {
                next();
            } else {
                return res.status(401).send({
                    message: errorHandler.getErrorMessage(null, 'Users', '100')
                });
            }
        }
    })(req, res, next);
};