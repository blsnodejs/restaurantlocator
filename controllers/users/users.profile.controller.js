'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors.js'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User');

/**
 * Update user details
 */
exports.update = function (req, res) {
    if (_.isUndefined(req.body.userId) || _.isNull(req.body.userId)) {
        return res.status(400)
            .send({
                message: errorHandler.getErrorMessage(null, 'Users', '114')
            });
    }
    var userId = req.body.userId;
    User.findById(userId, function (err, user) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        if (!user) {
            return res.send({
                message: errorHandler.getErrorMessage(null, 'Users', '110')
            });
        }

        // For security measurement we remove the roles from the req.body object
        delete req.body.roles;
        if (req.body._id) {
            delete req.body._id
        }

        // Merge existing user
        user = _.extend(user, req.body);
        user.updated = Date.now();
        user.displayName = user.firstName + ' ' + user.lastName;

        user.save(function (err, user) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
            return res.json(user);
        });
    });
};

exports.list = function (req, res) {
    User.find({roles: 'mobile_user'}, '-salt -password -resetPasswordToken -resetPasswordExpires -providerData.tokenSecret', function (err, users) {
        if (err) {
            return res
                .status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        if (!users || !users.length) {
            return res.send({
                message: errorHandler.getErrorMessage(null, 'Generic', '100')
            });
        }
        return res.json(users);
    });
};

exports.delete = function (req, res) {
    var userId = req.body.userId;

    if (_.isUndefined(userId) || _.isNull(userId)) {
        return res.status(400)
            .send({
                message: errorHandler.getErrorMessage(null, 'Users', '114')
            });
    }

    User.findByIdAndRemove(userId, function (err, user) {
        if (err) {
            return res.status(400)
                .send({
                    message: errorHandler.getErrorMessage(err)
                });
        }
        if (!user) {
            return res.send({
                message: errorHandler.getErrorMessage(null, 'Users', '110')
            });
        }
        return res.send(user);
    });
};

exports.getUserInfo = function (req, res) {
    var userRequested = req.profile;
    if (userRequested.password) {
        delete userRequested.password;
    }
    if (userRequested.salt) {
        delete userRequested.salt;
    }
    if (userRequested.resetPasswordToken) {
        delete userRequested.resetPasswordToken;
    }
    if (userRequested.resetPasswordExpires) {
        delete userRequested.resetPasswordExpires;
    }
    if (userRequested.providerData) {
        delete userRequested.providerData;
    }
    return res.send(userRequested);
};

/**
 * Send User
 */
exports.me = function (req, res) {
    res.json(req.user || null);
};