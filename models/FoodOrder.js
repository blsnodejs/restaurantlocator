'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FoodOrderSchema = new Schema({
    restaurantId: {
        type: Schema.Types.ObjectId,
        required: 'The restaurant id is required for food orders.',
        index: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: 'The user id is required for food orders.',
        index: true
    },
    dateOrdered: {
        type: 'Date',
        default: Date.now
    },
    orderedItems: [{
        name: 'String',
        price: 'Number',
        currency_code: 'String'
    }]

});

module.exports = mongoose.model('FoodOrder', FoodOrderSchema);