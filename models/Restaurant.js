'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RestaurantSchema = new Schema({
    name: {
        type: 'String',
        trim: true,
        required: 'Restaurant name is required.',
        index: true //we might search by restaurant name, let's index this for better performance when the database grows
    },
    cuisine: {
        type: 'String',
        set: function (cuisine) {
            /**
             * this is used to normalize all cuisine types
             */
            return cuisine.toLowerCase();
        }
    },
    menu: [{
        name: {
            type: 'String',
            required: 'The menu item name is required.'
        },
        description: 'String',
        notes: 'String',
        price: 'Number',
        currency_code: 'String'
    }],
    status: {
        type: 'Boolean',
        required: 'The restaurant status is required.',
        default: false
    }
});

module.exports = mongoose.model('Restaurant', RestaurantSchema);