'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RestaurantBranchSchema = new Schema({
    parentId: {
        type: Schema.Types.ObjectId,
        required: 'The ID of the parent restaurant is required.',
        index: true
    },
    name: {
        type: 'String',
        required: 'The branch name is required.'
    },
    loc: {
        'type': {
            type: 'String',
            default: 'Point'
        },
        coordinates: {
            type: [Number],
            default: [0,0]
        }
    }
});


RestaurantBranchSchema.index({loc: "2dsphere"});
module.exports = mongoose.model('RestaurantBranch', RestaurantBranchSchema);