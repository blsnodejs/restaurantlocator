'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    availableRatings = [1, 2, 3, 4, 5];

var RestaurantReviewSchema = new Schema({
    restaurantId: {
        type: Schema.Types.ObjectId,
        required: 'The restaurant id is required for food orders.',
        index: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: 'The user id is required for food orders.',
        index: true
    },
    rate: {
        type: 'Number',
        required: 'The user rate is required.',
        validate: [function (rating) {
            return (!isNaN(rating) && isFinite(rating) && availableRatings.indexOf(rating) !== -1)
        }, 'The supplied rate is not valid.']
    },
    description: 'String',
    dateReviewed: {
        type: 'Date',
        default: Date.now()
    }
});

module.exports = mongoose.model('RestaurantReview', RestaurantReviewSchema);