'use strict';

var foodOrders = require('../controllers/foodOrders'),
    users = require('../controllers/users');

module.exports = function (app) {
    app.route('/foodorders/user/:userId')
        .get(users.isMobileOrAdmin, foodOrders.getUserFoodOrders);

    app.route('/foodorders/restaurant/:restaurantId')
        .get(users.isAdminUser, foodOrders.getOrdersOfRestaurantId);

    app.route('/foodorder/:restaurantId/:userId')
        .post(users.isMobileOrAdmin, foodOrders.addFoodOrder);

    app.route('/foodorder')
        .delete(users.isAdminUser, foodOrders.deleteFoodOrder);

    app.route('/foodorder/:foodOrderId')
        .get(users.isAdminUser, foodOrders.getFoodOrderById);

    app.param('userId', users.userByID);
};