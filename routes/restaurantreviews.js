'use strict';

var restaurantReviews = require('../controllers/restaurantReviews'),
    users = require('../controllers/users');
var passport = require('passport');

module.exports = function (app) {
    app.route('/restaurantreview/:restaurantId')
        .get(users.isMobileOrAdmin, restaurantReviews.getReviewsByRestaurantId);

    app.route('/restaurantreview')
        .post(passport.authenticate('bearer', {session: false}), restaurantReviews.addRestaurantReview)
        .delete(users.isMobileOrAdmin, restaurantReviews.deleteRestaurantReview);
};