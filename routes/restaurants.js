'use strict';

var restaurants = require('../controllers/restaurants');
var users = require('../controllers/users');

module.exports = function (app) {
    app.route('/restaurants')
        .get(users.isMobileOrAdmin, restaurants.listEnabled)
        .post(users.isAdminUser, restaurants.create);

    app.route('/restaurants/all')
        .get(users.isAdminUser, restaurants.listAll);

    app.route('/restaurants/:restaurantId')
        .get(users.isMobileOrAdmin, restaurants.getRestaurantById)
        .put(users.isAdminUser, restaurants.update)
        .delete(users.isAdminUser, restaurants.delete);

    app.route('/branches/:restaurantId')
        .get(users.isMobileOrAdmin, restaurants.listBranches)
        .post(users.isAdminUser, restaurants.addBranch)
        .put(users.isAdminUser, restaurants.updateBranch)
        .delete(users.isAdminUser, restaurants.deleteBranch)

    app.route('/cuisines')
        .get(users.isMobileOrAdmin, restaurants.getAvailableCuisines);

    app.route('/cuisines/:cuisineType')
        .get(users.isMobileOrAdmin, restaurants.getRestaurantByCuisine);

    app.route('/proximity')
        .get(users.isMobileOrAdmin, restaurants.getRestaurantByProximity);

    app.param('restaurantId', restaurants.restaurantById);
};