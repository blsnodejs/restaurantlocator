'use strict';

var passport = require('passport');
var users = require('../controllers/users');

module.exports = function (app) {
    // Setting up the users profile api
    app.route('/users/me').get(users.me);
    app.route('/users')
        .get(users.isAdminUser, users.list)
        .put(users.isAdminUser, users.update)
        .delete(users.isAdminUser, users.delete);

    app.route('/users/:userId')
        .get(users.isAdminUser, users.getUserInfo);

    // Setting up the users password api
    app.route('/users/password').post(users.isAdminUser, users.changePassword);
    app.route('/auth/forgot').post(users.forgot);
    app.route('/auth/reset/:token')
        .get(users.validateResetToken)
        .post(users.reset);

    // Setting up the users authentication api
    /**
     * we will remove the signup route because we don't want users to signup, the admin might have the privilege
     * of adding users
     */
        //app.route('/auth/signup').post(users.signup);
    app.route('/auth/signin').post(users.signin);
    app.route('/auth/signout').get(users.signout);

    // Setting the facebook oauth routes
    app.route('/auth/facebook').get(passport.authenticate('facebook', {
        scope: ['email']
    }));
    app.route('/auth/facebook/callback').get(users.oauthCallback('facebook'));

    // Setting the twitter oauth routes
    app.route('/auth/twitter').get(passport.authenticate('twitter'));
    app.route('/auth/twitter/callback').get(users.oauthCallback('twitter'));

    // Setting the google oauth routes
    app.route('/auth/google').get(passport.authenticate('google', {
        scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ]
    }));
    app.route('/auth/google/callback').get(users.oauthCallback('google'));

    // Finish by binding the user middleware
    app.param('userId', users.userByID);
};