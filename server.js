'use strict';

var http = require('http');
var chalk = require('chalk');

var app = require('./app');
var config = require('./config/config');

var server = http.createServer(app);

server.listen(config.port, function onListen(err) {
    if (err) {
        console.error(chalk.red('Error starting http server'));
        throw err;
    }
    console.log(chalk.black.bgWhite('app started on port ' + config.port));
});