'use strict';

var app = require('../app'),
    errorHandler = require('../controllers/errors').rawMessages,
    mongoose = require('mongoose'),
    Restaurant = mongoose.model('Restaurant'),
    FoodOrder = mongoose.model('FoodOrder'),
    User = mongoose.model('User'),
    assert = require('assert');
var Session = require('supertest-session')({
    app: app,
    envs: {NODE_ENV: 'test'}
});

var restaurant, user, foodorder, mobileUser, credentials = {
    username: "fouad.kada",
    password: "thisisapassword"
};

describe('food orders tests', function () {
    before(function () {
        this.session = new Session();
    });

    after(function () {
        this.session.destroy();
    });

    beforeEach(function (done) {
        user = new User({
            firstName: 'Fouad',
            lastName: 'Kada',
            displayName: "Fouad Kada",
            email: "fouad.kada@gmail.com",
            username: "fouad.kada",
            password: "thisisapassword",
            roles: "admin",
            provider: "local",
            status: true
        });
        mobileUser = new User({
            firstName: 'Mobile',
            lastName: 'User',
            displayName: "Mobile User",
            email: "mobile.user@gmail.com",
            username: "mobile.user",
            provider: "google",
            status: true,
            providerData: {
                accessToken: 'ABC123'
            }
        });
        restaurant = new Restaurant({
            name: 'Test Restaurant',
            cuisine: 'burger',
            menu: [{
                name: 'Hamburger',
                description: 'meat, tomato, lettuce, ketchup, mustard, onion, pickles',
                notes: 'this is a specialty of the place',
                price: 10,
                currency_code: 'LBP'
            },
                {
                    name: 'Cheeseburger',
                    description: 'meat, cheese, tomato, lettuce, ketchup, mustard, onion, pickles',
                    notes: 'this is a specialty of the place but with cheese',
                    price: 11,
                    currency_code: 'USD'
                }],
            status: true
        });
        user.save(function (err) {
            if (err) return done(err);
            restaurant.save(function (err) {
                if (err) return done(err);
                foodorder = new FoodOrder({
                    restaurantId: restaurant._id,
                    userId: user._id,
                    orderedItems: [{
                        name: 'A Big Burger',
                        price: '10',
                        currency_code: 'USD'
                    }, {
                        name: 'A SMALL Burger',
                        price: '15000',
                        currency_code: 'LBP'
                    }]
                });
                foodorder.save(function (err) {
                    if (err) return done(err);
                    mobileUser.save(function (err) {
                        if (err) return done(err);
                        done();
                    });
                });
            });
        })
    });

    afterEach(function (done) {
        User.remove().exec();
        Restaurant.remove().exec();
        FoodOrder.remove().exec();
        done();
    });

    it('should not get all the orders by user id for unauthorized user', function (done) {
        this.session
            .get('/foodorders/user/' + user._id)
            .expect(401)
            .end(done);
    });

    it('should not get all the orders of a restaurant id for unauthorized user', function (done) {
        this.session
            .get('/foodorders/restaurant/' + restaurant._id)
            .expect(401)
            .end(done);
    });

    it('should not be able to successfully add a food order for an unauthorized user', function (done) {
        var newFoodOrder = {
            restaurantId: restaurant._id,
            userId: user._id,
            orderedItems: [{
                name: 'Shawarma',
                price: '4',
                currency_code: 'USD'
            }]
        };
        this.session
            .post('/foodorder/' + restaurant._id + '/' + user._id)
            .send(newFoodOrder)
            .expect(401)
            .end(done);
    });

    it('should not be able to delete a food order for unauthorized user', function (done) {
        this.session
            .del('/foodorder')
            .send({
                foodOrderId: foodorder._id
            })
            .expect(401)
            .expect('Content-Type', /json/)
            .end(done);
    });

    it('should not be able to get the food order information by providing a food order id for an unauthorized user', function (done) {
        this.session
            .get('/foodorder/' + foodorder._id)
            .expect(401)
            .end(done);
    });

    it('should not be able to get food orders of a restaurant but with no required admin role', function (done) {
        this.session
            .get('/foodorders/restaurant/' + restaurant._id)
            .query({
                access_token: mobileUser.providerData.accessToken
            })
            .expect(401)
            .end(done);
    });

    it('should be able to get food order user mobile user privileges', function (done) {
        this.session
            .get('/foodorders/user/' + mobileUser._id)
            .query({
                access_token: mobileUser.providerData.accessToken
            })
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Generic['100']);
                done();
            });
    });

    it('should be able to get food order user mobile user privileges', function (done) {
        var _this = this;
        foodorder = new FoodOrder({
            restaurantId: restaurant._id,
            userId: mobileUser._id,
            orderedItems: [{
                name: 'A Big Burger',
                price: '10',
                currency_code: 'USD'
            }]
        });
        foodorder.save(function (err) {
            if (err) return done(err);
            _this.session
                .get('/foodorders/user/' + mobileUser._id)
                .query({
                    access_token: mobileUser.providerData.accessToken
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    var response = res.body;
                    assert.strictEqual(true, Array.isArray(response));
                    assert.strictEqual(1, response.length);
                    done();
                });
        });
    });

    it('should get all the orders by user id', function (done) {
        var _this = this;
        this.session
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                _this.session
                    .get('/foodorders/user/' + user._id)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var ordersByUserId = res.body;
                        assert.strictEqual(true, Array.isArray(ordersByUserId));
                        assert.notEqual(0, ordersByUserId.length);
                        for (var i = 0; i < ordersByUserId.length; i++) {
                            var currentOrder = ordersByUserId[i];
                            assert.ok(currentOrder.restaurantId);
                            assert.ok(currentOrder.userId);
                            assert.ok(currentOrder.dateOrdered);
                            assert.ok(currentOrder.orderedItems);
                            assert.strictEqual(true, Array.isArray(currentOrder.orderedItems));
                        }
                        done();
                    });
            });
    });

    it('should get all the orders of a restaurant id', function (done) {
        var _this = this;
        this.session
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                _this.session
                    .get('/foodorders/restaurant/' + restaurant._id)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var ordersByRestaurant = res.body;
                        assert.strictEqual(true, Array.isArray(ordersByRestaurant));
                        assert.notEqual(0, ordersByRestaurant.length);
                        for (var i = 0; i < ordersByRestaurant.length; i++) {
                            var currentOrder = ordersByRestaurant[i];
                            assert.ok(currentOrder.restaurantId);
                            assert.ok(currentOrder.userId);
                            assert.ok(currentOrder.dateOrdered);
                            assert.ok(currentOrder.orderedItems);
                            assert.strictEqual(true, Array.isArray(currentOrder.orderedItems));
                        }
                        done();
                    });
            });
    });

    it('should be able to successfully add a food order', function (done) {
        var _this = this;
        var newFoodOrder = {
            restaurantId: restaurant._id,
            userId: user._id,
            orderedItems: [{
                name: 'Shawarma',
                price: '4',
                currency_code: 'USD'
            }]
        };
        this.session
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                _this.session
                    .post('/foodorder/' + restaurant._id + '/' + user._id)
                    .send(newFoodOrder)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var addedFoodOrder = res.body;
                        assert.strictEqual(addedFoodOrder.restaurantId, restaurant._id.toString());
                        assert.strictEqual(addedFoodOrder.userId, user._id.toString());
                        assert.ok(addedFoodOrder._id);
                        assert.strictEqual(true, Array.isArray(addedFoodOrder.orderedItems));
                        done();
                    });
            });
    });

    it('should be able to successfully add a food order with correct values', function (done) {
        var _this = this;
        var newFoodOrder = {
            restaurantId: 'some dummy values where the user is trying to get smart',
            userId: 'user trying to play some tricks',
            orderedItems: [{
                name: 'Shawarma',
                price: '4',
                currency_code: 'USD'
            }]
        };
        this.session
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                _this.session
                    .post('/foodorder/' + restaurant._id + '/' + user._id)
                    .send(newFoodOrder)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var addedFoodOrder = res.body;
                        assert.strictEqual(addedFoodOrder.restaurantId, restaurant._id.toString());
                        assert.strictEqual(addedFoodOrder.userId, user._id.toString());
                        assert.ok(addedFoodOrder._id);
                        assert.strictEqual(true, Array.isArray(addedFoodOrder.orderedItems));
                        done();
                    });
            });
    });

    it('should be able to delete a food order', function (done) {
        var _this = this;
        this.session
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                _this.session
                    .del('/foodorder')
                    .send({
                        foodOrderId: foodorder._id
                    })
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end(done);
            });
    });

    it('should not be able to delete a food order because of missing food order id', function (done) {
        var _this = this;
        this.session
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                _this.session
                    .del('/foodorder')
                    .expect(400)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var response = res.body;
                        assert.ok(response.message);
                        done();
                    });
            });
    });

    it('should be able to get the food order information by providing a food order id', function (done) {
        var _this = this;
        this.session
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                _this.session
                    .get('/foodorder/' + foodorder._id)
                    .expect(200)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var foodOrder = res.body;
                        assert.strictEqual(foodOrder.restaurantId, restaurant._id.toString());
                        assert.strictEqual(foodOrder.userId, user._id.toString());
                        assert.ok(foodOrder.dateOrdered);
                        assert.ok(foodOrder._id);
                        assert.strictEqual(true, Array.isArray(foodOrder.orderedItems));
                        done();
                    });
            });
    });
});