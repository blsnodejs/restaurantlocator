'use strict';

var app = require('../app'),
    errorHandler = require('../controllers/errors').rawMessages,
    mongoose = require('mongoose'),
    Restaurant = mongoose.model('Restaurant'),
    RestaurantReview = mongoose.model('RestaurantReview'),
    User = mongoose.model('User'),
    assert = require('assert');
var Session = require('supertest-session')({
    app: app,
    envs: {NODE_ENV: 'test'}
});

var restaurant, adminUser, mobileUser, restaurantReview, adminCredentials = {
    username: "fouad.kada",
    password: "thisisapassword"
};

describe('Restaurant Reviews tests', function () {
    before(function () {
        this.session = new Session();
    });

    after(function () {
        this.session.destroy();
    });

    beforeEach(function (done) {
        adminUser = new User({
            firstName: 'Fouad',
            lastName: 'Kada',
            displayName: "Fouad Kada",
            email: "fouad.kada@gmail.com",
            username: "fouad.kada",
            password: "thisisapassword",
            roles: "admin",
            provider: "local",
            status: true
        });
        mobileUser = new User({
            firstName: 'Mobile',
            lastName: 'User',
            displayName: "Mobile User",
            email: "mobile.user@local.com",
            username: "mobile.user",
            roles: "mobile_user",
            provider: "google",
            providerData: {
                accessToken: 'ABCTOKEN123'
            },
            status: true
        });
        restaurant = new Restaurant({
            name: 'Test Restaurant',
            cuisine: 'burger',
            menu: [{
                name: 'Hamburger',
                description: 'meat, tomato, lettuce, ketchup, mustard, onion, pickles',
                notes: 'this is a specialty of the place',
                price: 10,
                currency_code: 'LBP'
            },
                {
                    name: 'Cheeseburger',
                    description: 'meat, cheese, tomato, lettuce, ketchup, mustard, onion, pickles',
                    notes: 'this is a specialty of the place but with cheese',
                    price: 11,
                    currency_code: 'USD'
                }],
            status: true
        });
        adminUser.save(function (err) {
            if (err) return done(err);
            restaurant.save(function (err) {
                if (err) return done(err);
                restaurantReview = new RestaurantReview({
                    restaurantId: restaurant._id,
                    userId: mobileUser._id,
                    rate: 4,
                    description: 'this is one of the best restaurants ever'
                });
                restaurantReview.save(function (err) {
                    if (err) return done(err);
                    mobileUser.save(function (err) {
                        if (err) return done(err);
                        done();
                    });
                });
            });
        })
    });

    afterEach(function (done) {
        User.remove().exec();
        Restaurant.remove().exec();
        RestaurantReview.remove().exec();
        done();
    });

    it('should not get all the restaurant review by restaurant id for an unauthorized user', function (done) {
        this.session
            .get('/restaurantreview/' + restaurant._id)
            .expect(401)
            .end(done);
    });

    it('should get all the restaurant review by restaurant id', function (done) {
        var _this = this;
        _this.session
            .post('/auth/signin')
            .send({
                username: "fouad.kada",
                password: "thisisapassword"
            }).expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                _this.session
                    .get('/restaurantreview/' + restaurant._id)
                    .expect(200)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var theReviews = res.body;

                        assert.strictEqual(true, Array.isArray(theReviews));
                        assert.notEqual(0, theReviews.length);
                        done();
                    });
            });
    });

    it('should return an Empty Response Message.', function (done) {
        var _this = this;
        RestaurantReview.remove().exec(function (err) {
            if (err) return done();
            _this.session
                .post('/auth/signin')
                .send({
                    username: "fouad.kada",
                    password: "thisisapassword"
                }).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    _this.session
                        .get('/restaurantreview/' + restaurant._id)
                        .expect(200)
                        .end(function (err, res) {
                            if (err) return done(err);
                            var theReviews = res.body;

                            assert.strictEqual(false, Array.isArray(theReviews));
                            assert.strictEqual(theReviews.message, errorHandler.Generic['100']);
                            done();
                        });
                });
        });
    });

    it('should not get all the restaurant review by restaurant id for an unauthorized mobile user', function (done) {
        this.session
            .get('/restaurantreview/' + restaurant._id)
            .expect(401)
            .end(done);
    });

    it('should get all the restaurant review by restaurant id', function (done) {
        this.session
            .get('/restaurantreview/' + restaurant._id)
            .send({
                access_token: mobileUser.providerData.accessToken
            })
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var theReviews = res.body;

                assert.strictEqual(true, Array.isArray(theReviews));
                assert.notEqual(0, theReviews.length);

                done();
            });
    });

    it('should not be able to add a new Review because of unauthorized user', function (done) {
        this.session
            .post('/restaurantreview')
            .expect(401)
            .end(done);
    });

    it('should not be able to add a new Review because of missing restaurant id parameter', function (done) {
        this.session
            .post('/restaurantreview')
            .send({access_token: mobileUser.providerData.accessToken})
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var errorMessage = res.body;
                assert.ok(errorMessage.message);
                assert.strictEqual(errorMessage.message, errorHandler.RestaurantReviews['101']);
                done();
            });
    });

    it('should not be able to add a new Review because of missing user id parameter', function (done) {
        this.session
            .post('/restaurantreview')
            .send({
                restaurantId: restaurant._id,
                access_token: mobileUser.providerData.accessToken
            })
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var errorMessage = res.body;
                assert.ok(errorMessage.message);
                assert.strictEqual(errorMessage.message, errorHandler.RestaurantReviews['102']);
                done();
            });
    });

    it('should not be able to add a new Review because of missing rate parameter that is defined to be required in the Model definition', function (done) {
        this.session
            .post('/restaurantreview')
            .send({
                restaurantId: restaurant._id,
                userId: mobileUser._id,
                access_token: mobileUser.providerData.accessToken
            })
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var errorMessage = res.body;
                assert.ok(errorMessage.message);
                assert.strictEqual(errorMessage.message, errorHandler.Generic['101']);
                done();
            });
    });

    it('should not be able to delete a Review for unauthorized user', function (done) {
        this.session
            .del('/restaurantreview')
            .send({
                id: restaurantReview._id
            })
            .expect(401)
            .end(done);
    });

    it('should not be able to add a new Review because of an incorrect rate number supplied', function (done) {
        var restaurantReview = {
            restaurantId: restaurant._id,
            userId: mobileUser._id,
            rate: -13
        };
        this.session
            .post('/restaurantreview')
            .send(restaurantReview)
            .send({access_token: mobileUser.providerData.accessToken})
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var errorMessage = res.body;
                assert.ok(errorMessage.message);
                assert.strictEqual(errorMessage.message, errorHandler.Generic['101']);
                done();
            });
    });

    it('should not be able to delete a Review because of missing review id', function (done) {
        this.session
            .del('/restaurantreview')
            .send({access_token: mobileUser.providerData.accessToken})
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var errorMessage = res.body;
                assert.ok(errorMessage.message);
                assert.strictEqual(errorMessage.message, errorHandler.RestaurantReviews['103']);
                done();
            });
    });

    it('should not be able to delete a Review because of an incorrect review id provided', function (done) {
        this.session
            .del('/restaurantreview')
            .send({
                id: mobileUser._id
            })
            .send({access_token: mobileUser.providerData.accessToken})
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                var keys = Object.keys(response);
                assert.strictEqual(true, Array.isArray(keys));
                assert.strictEqual(0, keys.length);
                done();
            });
    });

    it('should be able to delete a Review', function (done) {
        this.session
            .del('/restaurantreview')
            .send({
                id: restaurantReview._id
            })
            .send({access_token: mobileUser.providerData.accessToken})
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;

                assert.strictEqual(response._id, restaurantReview._id.toString());
                assert.strictEqual(response.restaurantId, restaurantReview.restaurantId.toString());
                assert.strictEqual(response.userId, restaurantReview.userId.toString());
                assert.strictEqual(response.rate, restaurantReview.rate);
                assert.strictEqual(response.description, restaurantReview.description);
                RestaurantReview.count(function (err, count) {
                    if (err) return done(err);
                    assert.strictEqual(0, count);
                    done();
                });
            });
    });

    it('should be able to add a new Review', function (done) {
        var restaurantReview = {
            restaurantId: restaurant._id.toString(),
            userId: mobileUser._id.toString(),
            rate: 5,
            description: 'This is the best restaurant ever.'
        };
        this.session
            .post('/restaurantreview')
            .send(restaurantReview)
            .send({access_token: mobileUser.providerData.accessToken})
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var review = res.body;
                assert.ok(review._id);
                assert.strictEqual(review.restaurantId, restaurantReview.restaurantId);
                assert.strictEqual(review.userId, restaurantReview.userId);
                assert.strictEqual(review.rate, restaurantReview.rate);
                assert.strictEqual(review.description, restaurantReview.description);
                assert.ok(review.dateReviewed);
                done();
            });
    });
});