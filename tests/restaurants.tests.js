'use strict';

var app = require('../app'),
    mongoose = require('mongoose'),
    Restaurant = mongoose.model('Restaurant'),
    RestaurantBranch = mongoose.model('RestaurantBranch'),
    User = mongoose.model('User'),
    assert = require('assert');
var Session = require('supertest-session')({
    app: app,
    envs: {NODE_ENV: 'test'}
});

var restaurant, user, mobileUser;
var credentials = {
    username: 'fouad.kada',
    password: 'thisisapassword'
};

describe('Restaurants Tests', function () {
    before(function (done) {
        this.sess = new Session();
        user = new User({
            firstName: 'Fouad',
            lastName: 'Kada',
            displayName: "Fouad Kada",
            email: "fouad.kada@gmail.com",
            username: "fouad.kada",
            password: "thisisapassword",
            roles: "admin",
            provider: "local",
            status: true,
            providerData: {
                accessToken: '12345'
            }
        });
        user.save(function (err, user) {
            if (err) return done(err);
            mobileUser = new User({
                firstName: 'Mobile',
                lastName: 'User',
                displayName: "Mobile User",
                email: "mobile.user@local.com",
                username: "mobile.user",
                roles: "mobile_user",
                provider: "google",
                providerData: {
                    accessToken: 'ABCTOKEN123'
                },
                status: true
            });
            mobileUser.save(function (err) {
                if (err) return done(err);
                done();
            });
        });
    });

    after(function () {
        this.sess.destroy();
        User.remove().exec();
    });

    beforeEach(function (done) {
        restaurant = new Restaurant({
            name: 'Test Restaurant',
            cuisine: 'burger',
            menu: [{
                name: 'Hamburger',
                description: 'meat, tomato, lettuce, ketchup, mustard, onion, pickles',
                notes: 'this is a specialty of the place',
                price: 10,
                currency_code: 'LBP'
            },
                {
                    name: 'Cheeseburger',
                    description: 'meat, cheese, tomato, lettuce, ketchup, mustard, onion, pickles',
                    notes: 'this is a specialty of the place but with cheese',
                    price: 11,
                    currency_code: 'USD'
                }],
            status: true
        });

        restaurant.save(function (err, restaurant) {
            if (err) return done(err);
            done();
        });
    });

    afterEach(function (done) {
        Restaurant.remove().exec();
        RestaurantBranch.remove().exec();
        done();
    });

    it('should not be able to list restaurants after a GET for an unauthorized user', function (done) {
        this.sess
            .get('/restaurants')
            .expect(401)
            .end(done);
    });

    it('should not be able to create a new restaurant for an unauthorized user', function (done) {
        var newRestaurant = {
            name: 'Test Restaurant 2',
            cuisine: 'burger',
            menu: [{
                name: 'Alien Hamburger',
                description: 'meat, tomato, lettuce, ketchup, mustard, onion, pickles',
                notes: 'this is a specialty of the place',
                price: 100,
                currency_code: 'LBP'
            }],
            status: true
        };
        this.sess.post('/restaurants')
            .set('Content-Type', 'application/json')
            .send(newRestaurant)
            .expect(401)
            .end(done);
    });

    it('should not be able to update a restaurant using an unauthorized user', function (done) {
        var _this = this;
        var newRestaurant = new Restaurant({
            name: 'Test Restaurant 3',
            menu: [],
            cuisine: 'fusion',
            status: true
        });
        newRestaurant.save(function (err, restaurant) {
            if (err) return done(err);
            var newName = {name: 'new name'};
            _this.sess
                .put('/restaurants/' + restaurant._id)
                .set('Content-Type', 'application/json')
                .send(newName)
                .expect(401)
                .end(done);
        });
    });

    it('should not be able to delete a restaurant by id using an unauthorized user', function (done) {
        var _this = this;
        restaurant = new Restaurant({
            name: 'This is a restaurant that is gonna be deleted',
            status: true
        });

        restaurant.save(function (err, restaurant) {
            if (err) {
                return done(err);
            }
            _this.sess
                .del('/restaurants/' + restaurant._id)
                .expect(401)
                .end(done);
        });
    });

    it('should not be able to add a branch for a restaurant using an unauthorized user', function (done) {
        var _this = this;
        _this.sess
            .get('/restaurants')
            .expect(401)
            .end(done)
    });

    it('should not be able to update a branch of a restaurant using an unauthorized user', function (done) {
        var _this = this;
        var branchesNames = ['Branch 1', 'Branch 2'];
        var restaurantId = restaurant._id;
        var restaurantBranch = new RestaurantBranch({
            name: branchesNames[0],
            parentId: restaurantId
        });
        restaurantBranch.save(function (err, restaurantBranch1) {
            if (err) return done(err);
            restaurantBranch = new RestaurantBranch({
                name: branchesNames[1],
                parentId: restaurantId
            });
            restaurantBranch.save(function (err) {
                var updatedBranchInfo = {
                    _id: restaurantBranch1._id.toString(),
                    name: 'This is the new Name'
                };
                if (err) return done();
                _this.sess
                    .put('/branches/' + restaurantId)
                    .send(updatedBranchInfo)
                    .expect(401)
                    .end(done);
            });
        });
    });

    it('should not be able to search for branches by proximity for unauthorized users', function (done) {
        var _this = this;
        var restaurantId = restaurant._id;
        var restaurantBranch = new RestaurantBranch({
            name: 'Beirut Branch',
            parentId: restaurantId,
            loc: {
                type: 'Point',
                coordinates: [35.495479, 33.888629]
            }
        });
        restaurantBranch.save(function (err) {
            if (err) return done(err);
            _this.sess
                .get('/proximity')
                .query({
                    long: 0,
                    lat: 0,
                    proximityRange: 1
                })
                .expect(401)
                .end(done);
        });
    });

    it('should be able to search for branches by proximity for a mobile user users', function (done) {
        var _this = this;
        var restaurantId = restaurant._id;
        var restaurantBranch = new RestaurantBranch({
            name: 'Beirut Branch',
            parentId: restaurantId,
            loc: {
                type: 'Point',
                coordinates: [35.495479, 33.888629]
            }
        });
        restaurantBranch.save(function (err) {
            if (err) return done(err);
            _this.sess
                .get('/proximity')
                .query({
                    lat: 33.921346,
                    long: 35.588986,
                    proximityRange: 10000,
                    access_token: mobileUser.providerData.accessToken
                })
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    var response = res.body;

                    assert.strictEqual(true, Array.isArray(response));
                    assert.notEqual(0, response.length);
                    done();
                });
        });
    });

    it('should be able to sign in', function (done) {
        this.sess
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(done);
    });


    it('should be able to update a branch of a restaurant', function (done) {
        var _this = this;
        var branchesNames = ['Branch 1', 'Branch 2'];
        var restaurantId = restaurant._id;
        var restaurantBranch = new RestaurantBranch({
            name: branchesNames[0],
            parentId: restaurantId
        });
        restaurantBranch.save(function (err, restaurantBranch1) {
            if (err) return done(err);
            restaurantBranch = new RestaurantBranch({
                name: branchesNames[1],
                parentId: restaurantId
            });
            restaurantBranch.save(function (err) {
                var updatedBranchInfo = {
                    _id: restaurantBranch1._id.toString(),
                    name: 'This is the new Name'
                };
                if (err) return done();
                _this.sess
                    .put('/branches/' + restaurantId)
                    .send(updatedBranchInfo)
                    .expect(200)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var updatedBranch = res.body;
                        assert.strictEqual(updatedBranch.name, updatedBranchInfo.name);
                        done();
                    });
            });
        });
    });

    it('should be able to create a new restaurant', function (done) {
        var _this = this;
        var newRestaurant = {
            name: 'Test Restaurant 2',
            cuisine: 'burger',
            menu: [{
                name: 'Alien Hamburger',
                description: 'meat, tomato, lettuce, ketchup, mustard, onion, pickles',
                notes: 'this is a specialty of the place',
                price: 100,
                currency_code: 'LBP'
            }],
            status: true
        };

        _this.sess
            .post('/restaurants')
            .set('Content-Type', 'application/json')
            .send(newRestaurant)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }

                var body = res.body;

                assert.strictEqual(body.name, newRestaurant.name);
                assert.strictEqual(Object.prototype.toString.call(body.branches), Object.prototype.toString.call(newRestaurant.branches));
                assert.strictEqual(body.cuisine, newRestaurant.cuisine);
                assert.strictEqual(body.menu.length, newRestaurant.menu.length);
                for (var i = 0; i < body.menu.length; i++) {
                    assert.strictEqual(body.menu[i].name, newRestaurant.menu[i].name);
                    assert.strictEqual(body.menu[i].description, newRestaurant.menu[i].description);
                    assert.strictEqual(body.menu[i].notes, newRestaurant.menu[i].notes);
                    assert.strictEqual(body.menu[i].price, newRestaurant.menu[i].price);
                    assert.strictEqual(body.menu[i].currency_code, newRestaurant.menu[i].currency_code);
                }
                assert.strictEqual(body.status, newRestaurant.status);

                /**
                 * this is to make sure that the newly added restaurant has an _id
                 */
                assert.doesNotThrow(function () {
                    return body._id;
                });

                done();
            });
    });

    it('should update a restaurant name using an authorized user', function (done) {
        var _this = this;
        var newRestaurant = new Restaurant({
            name: 'Test Restaurant 3',
            menu: [],
            cuisine: 'fusion',
            status: true
        });
        var newName = {name: 'update test restaurant 4'};
        newRestaurant.save(function (err, restaurant) {
            if (err) return done(err);

            _this.sess
                .put('/restaurants/' + restaurant._id)
                .set('Content-Type', 'application/json')
                .send(newName)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        return done(err);
                    }

                    var body = res.body;

                    assert.strictEqual(body.name, newName.name);
                    done();
                });
        });
    });

    it('should be able to add a branch for a restaurant', function (done) {
        var _this = this;
        _this.sess
            .get('/restaurants')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function (err, res) {
                if (err) return done(err);
                var restaurants = res.body;

                var currentRestaurant = restaurants[0];
                var newBranch = {
                    name: 'A new branch branch'
                };
                _this.sess
                    .post('/branches/' + currentRestaurant._id)
                    .send(newBranch)
                    .expect(200)
                    .end(function (err, response) {
                        if (err) return done(err);
                        var body = response.body;
                        assert.strictEqual(body.parentId, currentRestaurant._id);
                        assert.strictEqual(body.name, newBranch.name);
                        done();
                    });
            })
    });

    it('should be able to list branches of a restaurant', function (done) {
        var _this = this;
        var branchesNames = ['Branch 1', 'Branch 2'];
        var restaurantId = restaurant._id;
        var restaurantBranch = new RestaurantBranch({
            name: branchesNames[0],
            parentId: restaurantId
        });
        restaurantBranch.save(function (err) {
            if (err) return done(err);
            restaurantBranch = new RestaurantBranch({
                name: branchesNames[1],
                parentId: restaurantId
            });
            restaurantBranch.save(function (err) {
                if (err) return done(err);
                _this.sess
                    .get('/branches/' + restaurantId)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var branches = res.body;
                        assert.notEqual(0, branches.length);
                        assert.strictEqual(branchesNames.length, branches.length);
                        done();
                    });
            });
        });
    });

    it('should be able to delete a branch of a restaurant', function (done) {
        var _this = this;
        var branchesNames = ['Branch 1', 'Branch 2'];
        var restaurantId = restaurant._id;
        var restaurantBranch = new RestaurantBranch({
            name: branchesNames[0],
            parentId: restaurantId
        });
        restaurantBranch.save(function (err, restaurantBranch1) {
            if (err) return done(err);
            restaurantBranch = new RestaurantBranch({
                name: branchesNames[1],
                parentId: restaurantId
            });
            restaurantBranch.save(function (err) {
                if (err) return done(err);
                _this.sess
                    .del('/branches/' + restaurantId)
                    .send(restaurantBranch1)
                    .expect(200)
                    .end(function (err, res) {
                        if (err) return done(err);
                        var deletedBranch = res.body;
                        assert(deletedBranch._id, restaurantBranch1._id.toString());

                        _this.sess
                            .get('/branches/' + restaurantId)
                            .expect(200)
                            .expect('Content-Type', /json/)
                            .end(function (err, res) {
                                if (err) return done(err);

                                var newBranches = res.body;
                                assert.strictEqual(-1, newBranches.indexOf(deletedBranch));
                                done();
                            });
                    });
            });
        });
    });

    it('should be able to delete a restaurant by id', function (done) {
        var _this = this;
        restaurant = new Restaurant({
            name: 'This is a restaurant that is gonna be deleted',
            status: true
        });

        restaurant.save(function (err, restaurant) {
            if (err) {
                return done(err);
            }
            _this.sess
                .del('/restaurants/' + restaurant._id)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        return done(err);
                    }
                    var body = res.body;
                    assert.strictEqual(body.name, restaurant.name);
                    assert.strictEqual(body.status, restaurant.status);
                    assert.strictEqual(body._id, restaurant._id.toString());
                    done();
                });
        });
    });

    it('should be able to return restaurants with specific cuisine type', function (done) {
        var _this = this;
        var cuisineType = 'Lebanese';
        restaurant = new Restaurant({
            name: 'This is a Lebanese Cuisine restaurant',
            cuisine: cuisineType,
            status: true
        });
        restaurant.save(function (err, restaurant) {
            if (err) return done(err);
            var anotherRestaurant = new Restaurant({
                name: 'This is a Lebanese Cuisine restaurant',
                cuisine: cuisineType,
                status: true
            });
            anotherRestaurant.save(function (err, restaurant) {
                if (err) return done(err);
                _this.sess
                    .get('/cuisines/lebanese')
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end(function (err, res) {
                        if (err) return done(err);

                        var restaurants = res.body;
                        assert.notEqual(0, restaurants.length);
                        for (var i = 0; i < restaurants.length; i++) {
                            var currentRestaurant = restaurants[i];
                            assert(currentRestaurant.cuisine, cuisineType.toLowerCase());
                        }
                        done();
                    });
            });
        });
    });

    it('should be able to get a list of available cuisines', function (done) {
        this.sess
            .get('/cuisines')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function (err, res) {
                if (err) return done(err);

                var cuisines = res.body;
                assert.notEqual(0, cuisines.length);
                done();
            });
    });

    it('should be able to list restaurants after a GET', function (done) {
        this.sess
            .get('/restaurants')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }

                var body = res.body;

                assert.notEqual(0, body.length);
                for (var i = 0; i < body.length; i++) {
                    var currentRestaurant = body[i];
                    assert.strictEqual(currentRestaurant.name, restaurant.name);
                    assert.strictEqual(Object.prototype.toString.call(currentRestaurant.branches), Object.prototype.toString.call(restaurant.branches));
                    assert.strictEqual(currentRestaurant.cuisine, restaurant.cuisine);
                    assert.strictEqual(currentRestaurant.menu.length, restaurant.menu.length);
                    for (var j = 0; j < currentRestaurant.menu.length; j++) {
                        assert.strictEqual(currentRestaurant.menu[j].name, restaurant.menu[j].name);
                        assert.strictEqual(currentRestaurant.menu[j].description, restaurant.menu[j].description);
                        assert.strictEqual(currentRestaurant.menu[j].notes, restaurant.menu[j].notes);
                        assert.strictEqual(currentRestaurant.menu[j].price, restaurant.menu[j].price);
                        assert.strictEqual(currentRestaurant.menu[j].currency_code, restaurant.menu[j].currency_code);
                    }
                    assert.strictEqual(currentRestaurant.status, restaurant.status);
                }
                done();
            });
    });

    it('should be able to list all restaurants', function (done) {
        var _this = this;
        restaurant = new Restaurant({
            name: 'Disabled Restaurant',
            cuisine: 'burger',
            menu: [{
                name: 'Hamburger',
                description: 'meat, tomato, lettuce, ketchup, mustard, onion, pickles',
                notes: 'this is a specialty of the place',
                price: 10,
                currency_code: 'LBP'
            }],
            status: false
        });
        restaurant.save(function (err) {
            if (err) return done(err);
            _this.sess
                .get('/restaurants/all')
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function (err, res) {
                    if (err) {
                        return done(err);
                    }

                    var body = res.body;

                    assert.notEqual(0, body.length);
                    assert.strictEqual(2, body.length);
                    done();
                });
        });
    });

    it('should be able to search for branches by proximity', function (done) {
        var _this = this;
        var restaurantId = restaurant._id;
        var restaurantBranch = new RestaurantBranch({
            name: 'Beirut Branch',
            parentId: restaurantId,
            loc: {
                type: 'Point',
                coordinates: [35.495479, 33.888629]
            }
        });
        restaurantBranch.save(function (err) {
            if (err) return done(err);
            _this.sess
                .get('/proximity')
                .query({
                    long: 0,
                    lat: 0,
                    proximityRange: 1
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function (err, res) {
                    if (err) return done(err);
                    var nearByBranches = res.body;
                    assert.equal(0, nearByBranches.length);

                    _this.sess
                        .get('/proximity')
                        .query({
                            lat: 33.921346,
                            long: 35.588986,
                            proximityRange: 10000
                        })
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .end(function (err, res) {
                            if (err) return done(err);

                            var nearByBranches = res.body;
                            assert.notEqual(0, nearByBranches.length);
                            done();
                        });
                });
        });
    });
});