'use strict';

var app = require('../app'),
    errorHandler = require('../controllers/errors').rawMessages,
    request = require('supertest').agent(app),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    assert = require('assert');
var Session = require('supertest-session')({
    app: app,
    envs: {NODE_ENV: 'test'}
});


var user, mobileUser;
var credentials = {
    username: 'fouad.kada',
    password: 'thisisapassword'
};

describe('User routes tests', function () {
    before(function (done) {
        this.sess = new Session();
        user = new User({
            firstName: 'Fouad',
            lastName: 'Kada',
            displayName: "Fouad Kada",
            email: "fouad.kada@gmail.com",
            username: "fouad.kada",
            password: "thisisapassword",
            roles: "admin",
            provider: "local",
            status: true
        });
        user.save(function (err, user) {
            if (err) return done(err);
            done();
        });
    });

    beforeEach(function (done) {
        mobileUser = new User({
            firstName: 'Mobile',
            lastName: 'User',
            displayName: "Mobile User",
            email: "mobile.user@gmail.com",
            username: "new.mobile.user",
            roles: "mobile_user",
            provider: "google",
            status: true
        });
        mobileUser.save(function (err) {
            if (err) return done(err);
            done();
        });
    });

    afterEach(function (done) {
        User.remove({'roles': 'mobile_user'}, function (err) {
                if (err)return done(err);
                done();
            }
        )
    });

    after(function () {
        this.sess.destroy();
        User.remove().exec();
    });

    it('should be able to sign in', function (done) {
        this.sess
            .post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;

                /**
                 * when loggin in a cookie is set, we should check for its existence
                 */
                assert.notEqual(0, res.headers['set-cookie'].length);

                assert.strictEqual(response.displayName, user.displayName);
                assert.strictEqual(response.provider, user.provider);
                assert.strictEqual(response.username, user.username);
                done();
            });
    });

    it('should get an empty response message in case no users already exists', function (done) {
        var _this = this;
        User.remove({'roles': 'mobile_user'}, function (err) {
            if (err) return done(err);
            _this.sess
                .get('/users')
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);

                    var response = res.body;
                    assert.ok(response.message);
                    assert.strictEqual(response.message, errorHandler.Generic['100']);
                    done();
                });
        });
    });

    it('should be able to get a list of available users', function (done) {
        this.sess.get('/users')
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;

                assert.notEqual(0, response.length);
                done();
            });
    });

    it('should be able to update a user', function (done) {
        var newInformationToProvide = {
            _id: user._id, //we will provide this to simulate someone truing to provide another _id to modify
            userId: mobileUser._id, //this is the id that will be searched for,
            firstName: 'New Mobile',
            lastName: 'New User'
        };
        this.sess
            .put('/users')
            .send(newInformationToProvide)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;

                assert.strictEqual(response._id, newInformationToProvide.userId.toString());
                assert.strictEqual(response.firstName, newInformationToProvide.firstName);
                assert.strictEqual(response.lastName, newInformationToProvide.lastName);
                done();
            });
    });

    it('should not be able to update a user because of missing userId parameter', function (done) {
        var newInformationToProvide = {
            _id: user._id, //we will provide this to simulate someone truing to provide another _id to modify
            firstName: 'New Mobile',
            lastName: 'New User'
        };
        this.sess
            .put('/users')
            .send(newInformationToProvide)
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;

                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['114']);
                done();
            });
    });

    it('should not be able to update a user because of a userId not found in database', function (done) {
        var newInformationToProvide = {
            _id: user._id, //we will provide this to simulate someone truing to provide another _id to modify
            userId: '550456eff4a260d65b5bdbef', //this is an example ObjectId that should not be in the database
            firstName: 'New Mobile',
            lastName: 'New User'
        };
        this.sess
            .put('/users')
            .send(newInformationToProvide)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;

                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['110']);
                done();
            });
    });

    it('should be able to delete a user', function (done) {
        this.sess.del('/users')
            .send({userId: mobileUser._id})
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.strictEqual(response._id, mobileUser._id.toString());
                User.count({'roles': 'mobile_user'}, function (err, count) {
                    if (err) return done();
                    assert.strictEqual(0, count);
                    done();
                });
            });
    });

    it('should not be able to delete a user because of missing userId parameter', function (done) {
        this.sess.del('/users')
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['114']);
                User.count({'roles': 'mobile_user'}, function (err, count) {
                    if (err) return done();
                    assert.strictEqual(1, count);
                    done();
                });
            });
    });

    it('should not be able to delete a user because of a userId not found in database', function (done) {
        var userId = '550456eff4a260d65b5bdbef'; //this is an example ObjectId
        this.sess.del('/users')
            .send({userId: userId})
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['110']);
                User.count({'roles': 'mobile_user'}, function (err, count) {
                    if (err) return done();
                    assert.strictEqual(1, count);
                    done();
                });
            });
    });

    it('should be able to change a password', function (done) {
        var infoToSend = {
            currentPassword: 'thisisapassword',
            newPassword: 'thisisthenewpassword',
            verifyPassword: 'thisisthenewpassword'
        };
        this.sess
            .post('/users/password')
            .send(infoToSend)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['113']);
                done();
            });
    });

    it('should not be able to change a password because of incorrect current password', function (done) {
        var infoToSend = {
            currentPassword: 'incorrectcurrentpassword',
            newPassword: 'thisisthenewpassword',
            verifyPassword: 'thisisthenewpassword'
        };
        this.sess
            .post('/users/password')
            .send(infoToSend)
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['111']);
                done();
            });
    });

    it('should not be able to change a password because of newpassword and verifypassword do not match', function (done) {
        var infoToSend = {
            currentPassword: 'thisisthenewpassword',
            newPassword: '123',
            verifyPassword: '456'
        };
        this.sess
            .post('/users/password')
            .send(infoToSend)
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['112']);
                done();
            });
    });

    it('should be able to get user information by user id', function (done) {
        this.sess
            .get('/users/' + mobileUser._id)
            .expect(200)
            .end(done);
    });

    it('should be able to logout', function (done) {
        this.sess.get('/auth/signout')
            .expect(302)
            .end(function (err, res) {
                if (err) return done(err);

                /**
                 * after logging out, the cookie should not exist anymore
                 */
                assert.strictEqual(undefined, res.headers['set-cookie']);
                done();
            });
    });

    it('should not be able to change the password for an unauthorized user', function (done) {
        var infoToSend = {
            currentPassword: 'thisisapassword',
            newPassword: 'thisisthenewpassword',
            verifyPassword: 'thisisthenewpassword'
        };
        this.sess
            .post('/users/password')
            .send(infoToSend)
            .expect(401)
            .end(done);
    });

    it('should not be able to get a list of available users', function (done) {
        this.sess.get('/users')
            .expect(401)
            .end(done);
    });

    it('should be not be able to update a user for an unauthorized user', function (done) {
        var _this = this;
        var mobileUser = new User({
            firstName: 'Mobile',
            lastName: 'User',
            displayName: "Mobile User",
            email: "mobile.user@gmail.com",
            username: "new.mobile.user.1",
            roles: "mobile_user",
            provider: "google",
            status: true
        });
        mobileUser.save(function (err) {
            if (err) return done(err);
            var newInformationToProvide = {
                _id: user._id, //we will provide this to simulate someone truing to provide another _id to modify
                userId: mobileUser._id, //this is the id that will be searched for,
                firstName: 'New Mobile',
                lastName: 'New User'
            };
            _this.sess
                .put('/users')
                .send(newInformationToProvide)
                .expect(401)
                .end(done);
        });
    });

    it('should be able to request a token for reset a password', function (done) {
        this.sess
            .post('/auth/forgot')
            .send({username: user.username})
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.notEqual(-1, response.message.indexOf('You should visit'));
                assert.ok(response.token);
                done();
            });
    });

    it('should not be able to request a token for reset a password because of missing username parameter', function (done) {
        this.sess
            .post('/auth/forgot')
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['102']);
                done();
            });
    });

    it('should not be able to request a token for reset a password because of username not found', function (done) {
        this.sess
            .post('/auth/forgot')
            .send({username: 'someusernamethatwillnotbefound'})
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.strictEqual(response.message, errorHandler.Users['115']);
                done();
            });
    });


    it('should not be able to request a token for reset a password because of using oAuth to register', function (done) {
        this.sess
            .post('/auth/forgot')
            .send({username: mobileUser.username})
            .expect(400)
            .end(function (err, res) {
                if (err) return done(err);
                var response = res.body;
                assert.ok(response.message);
                assert.notEqual(-1, response.message.indexOf('It seems like you signed up using your'));
                done();
            });
    });
});